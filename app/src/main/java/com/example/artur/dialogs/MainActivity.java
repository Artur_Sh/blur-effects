package com.example.artur.dialogs;

import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements GestureDetector.OnGestureListener {

    private GestureDetectorCompat mDetector;
    LinearLayout linearLayout;
    SeekBarExtension sbe;
    ImageView imageView;
    Button loadButton;
    EditText editText;
    String url;
    float width;
    int min, max;
    int range;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (LinearLayout) findViewById(R.id.baseLayout);
        sbe = (SeekBarExtension) findViewById(R.id.seekBarExt);
        imageView = (ImageView) findViewById(R.id.imageView);
        loadButton =(Button) findViewById(R.id.loadButton);
        editText = (EditText) findViewById(R.id.editText);
        mDetector = new GestureDetectorCompat(this, this);
        min = 0; max = 100; range = max - min;
        sbe.setRange(min, max);
        sbe.setProgress(range);
        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.loadButton) {
                    url = editText.getText().toString();
                    new DownloadImageTask(imageView, MainActivity.this).execute(url);
                    imageView.setAlpha(sbe.getProgress()/range);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
    }

    public void onSaveInstanceState(Bundle saveState) {
        super.onSaveInstanceState(saveState);
        saveState.putFloat("value", sbe.getProgress());
        saveState.putFloat("alpha", imageView.getAlpha());
    }

    public void onRestoreInstanceState(Bundle restoreState) {
        super.onRestoreInstanceState(restoreState);
        sbe.setProgress(restoreState.getFloat("value"));
        url = editText.getText().toString();
        new DownloadImageTask(imageView, MainActivity.this).execute(url);
        imageView.setAlpha(restoreState.getFloat("alpha"));
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        Toast.makeText(MainActivity.this, "Just a Tap", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        sbe.addProgress(range * (-v)/width);
        imageView.setAlpha(sbe.getProgress()/ range);
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {
        Toast.makeText(MainActivity.this, "It's a LongPress Gesture", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mDetector.onTouchEvent(event);
    }
}