package com.example.artur.dialogs;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    ImageView imageView;
    Context ctx;

    DownloadImageTask(ImageView imageView, Context ctx) {
        this.imageView = imageView;
        this.ctx = ctx;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        Bitmap image = null;
        try {
            InputStream is = new URL(url).openStream();
            image = BitmapFactory.decodeStream(is);
        }
        catch (Exception e) {
            System.out.println("No such URL exists");
        }
        return image;
    }

    protected void onPostExecute(Bitmap result) {
        if(result == null) {
            imageView.setImageBitmap(null);
            Toast.makeText(ctx, "No such URL exists", Toast.LENGTH_SHORT).show();
        }
        else
            imageView.setImageBitmap(result);
    }
}
