package com.example.artur.dialogs;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class SeekBarExtension extends LinearLayout {

    private float progress;
    private SeekBar sb1;
    private SeekBar sb2;
    private TextView tv1;
    private TextView tv2;
    private int min;
    private int max;
    private int shift;
    private LinearLayout linearLayout;
    private LinearLayout linearLayoutText;
    private LayoutParams layoutParams1;
    private LayoutParams layoutParams2;

    SeekBarExtension(Context ctx, AttributeSet attr_set) {
        super(ctx, attr_set);
        TypedArray a = ctx.getTheme().obtainStyledAttributes(attr_set, R.styleable.SeekBarExtension, 0, 0);
        progress = a.getInt(R.styleable.SeekBarExtension_progress, 0);
        a.recycle();

        inflate(getContext(), R.layout.extension_layout, this);  // ????

        sb1 = (SeekBar) findViewById(R.id.seekBarNeg);
        sb2 = (SeekBar) findViewById(R.id.seekBarPos);
        tv1 = (TextView) findViewById(R.id.textView);
        tv2 = (TextView) findViewById(R.id.textView2);

        sb1.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        sb2.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        linearLayoutText = (LinearLayout) findViewById(R.id.linearLayoutText);
        layoutParams1 = (LinearLayout.LayoutParams) sb1.getLayoutParams();
        layoutParams2 = (LinearLayout.LayoutParams) sb2.getLayoutParams();
    }

    public void setRange(int min, int max) {
        this.min = min;
        this.max = max;

        if(min >= 0) {
            linearLayout.removeView(sb1);
            linearLayoutText.removeView(tv1);
            sb2.setMax(max - min);
            shift = -min;
        }
        else if (max <= 0) {
            linearLayout.removeView(sb2);
            linearLayoutText.removeView(tv2);
            sb1.setMax(max - min);
            shift = max;
        }
        else {  // if(min <= 0 && max >= 0)
            sb1.setMax(-min);
            sb2.setMax(max);
            layoutParams1.weight = -min;
            layoutParams2.weight = max;
            shift = 0;
        }

        progress = (max + min)/2;
        displayProgress();
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float p) {
        progress = p;
        displayProgress();
    }

    public void displayProgress() {
        if(progress >= 0) {
            sb1.setProgress(0);
            sb2.setProgress((int)(progress + shift));
            tv2.setText(String.valueOf((int)progress));
        }
        if (progress <= 0) {
            sb1.setProgress((int)(-progress + shift));
            sb2.setProgress(0);
            tv1.setText(String.valueOf((int)progress));
        }
    }

    public void addProgress(float delta) {
        progress += delta;
        if(progress > max) {
            progress = max;
        }
        else if(progress < min) {
            progress = min;
        }
        displayProgress();
    }
}